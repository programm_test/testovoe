<?php

include_once 'inc/post-types.php';
include_once 'inc/assets.php';


add_filter('rwmb_meta_boxes', 'your_prefix_register_meta_boxes');

function your_prefix_register_meta_boxes($meta_boxes)
{
    $prefix = '';

    $meta_boxes[] = [
        'title' => esc_html__('Выбор города'),
        'id' => 'untitled',
        'context' => 'normal',
        'post_types' => 'realestate',
        'priority' => 'low',
        'fields' => [
            [
                'type' => 'post',
                'name' => esc_html__('Города'),
                'id' => $prefix . 'metacity',
                'post_type' => 'city',
                'field_type' => 'select_advanced',
            ],
        ],
    ];

    return $meta_boxes;
}

add_action('wp_ajax_nopriv_add_realestate', 'add_realestate');
add_action('wp_ajax_add_realestate', 'add_realestate');
function add_realestate()
{

    // Знаю что тут нет  ни проверок на дубли и сомнительно с точки зрения безопастности
    $data = $_POST;
    $user_id = get_current_user_id();

    if (isset($data)) {
        $post_id = wp_insert_post(wp_slash(array(
            'post_type' => 'realestate',
            'post_status' => 'publish',
            'post_author' => $user_id,
            'ping_status' => get_option('default_ping_status'),
            'post_title' => $data['title'],
            'meta_input' => [
                'area' => $data['area'],
                'price' => $data['price'],
                'address' => $data['address'],
                'living_space' => $data['living_space'],
                'floor' => $data['floor'],
                'metacity' => $data['city'],
            ],
        )));
        wp_set_post_terms($post_id, $data['property'], 'property-type');
        $id = media_handle_sideload($_FILES['image'], $post_id, $desc = null);
        if (is_wp_error($id)) {
            var_dump($id->get_error_messages());
        } else {
            update_post_meta($post_id, '_thumbnail_id', $id);
        }
        $realestate = get_posts(array(
            'numberposts' => 20,
            'category' => 0,
            'orderby' => 'date',
            'order' => 'DESC',
            'post_type' => 'realestate',
            'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
        ));
        ?>

        <?php

        if ($realestate) {
            foreach ($realestate as $post) {
                ob_start();
                get_template_part('loop-templates/content-realestate', get_post_format($post->ID),array('post'=>$post));
                $html .= ob_get_contents();
                ob_clean();
            }
        } else {
            get_template_part('loop-templates/content', 'none');
        }
        $result['success'] = true;
        $result['html'] = $html;
        echo json_encode($result);

        wp_die();



    }

}

function wx_debug($elem)
{
    echo '<pre>' . print_r($elem, 1) . '</pre>';
}