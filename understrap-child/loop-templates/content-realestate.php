<?php
/**
 * Post rendering content according to caller of get_template_part
 *
 * @package Understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if( isset($args['post'])){
  $post = $args['post'];
}
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<header class="entry-header">

		<?php

		the_title(
			sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
			'</a></h2>'
		);
		?>

		<?php if ( 'post' === get_post_type() ) : ?>

			<div class="entry-meta">
				<?php understrap_posted_on(); ?>
			</div><!-- .entry-meta -->

		<?php endif; ?>

	</header><!-- .entry-header -->



	<div class="entry-content row">
        <div class="image-block col-6"><?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?></div>
        <ul class="characteristics list-group col-6">
            <li class="characteristics-item list-group-item ">
                Площадь: <b><?php the_field('area');?></b> кв.м
            </li>
            <li class="characteristics-item  list-group-item  ">
                Стоимость:
               <b> <?php the_field('price');?></b> руб.
            </li>
            <li class="characteristics-item list-group-item ">
                Адрес:
                <b><?php the_field('address');?></b>
            </li>
            <li class="characteristics-item list-group-item  ">
               Жилая площадь:
                <b><?php the_field('living_space');?> </b> кв.м
            </li>
            <li class="characteristics-item list-group-item">
               Этаж:
                <b><?php the_field('floor');?></b>
            </li>
        </ul>


	</div><!-- .entry-content -->



</article><!-- #post-## -->
