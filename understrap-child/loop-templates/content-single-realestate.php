<?php
/**
 * Single post partial template
 *
 * @package Understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<header class="entry-header">

		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<div class="entry-meta">
			<?php understrap_posted_on(); ?>

		</div><!-- .entry-meta -->

	</header><!-- .entry-header -->

	<?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?>

	<div class="entry-content " >
        <ul class="characteristics list-group">
            <li class="characteristics-item list-group-item ">
                Площадь: <b><?php the_field('area');?></b> кв.м
            </li>
            <li class="characteristics-item  list-group-item  ">
                Стоимость:
               <b> <?php the_field('price');?></b> руб.
            </li>
            <li class="characteristics-item list-group-item ">
                Адрес:
                <b><?php the_field('address');?></b>
            </li>
            <li class="characteristics-item list-group-item  ">
               Жилая площадь:
                <b><?php the_field('living_space');?> </b> кв.м
            </li>
            <li class="characteristics-item ">
               Этаж:
                <b><?php the_field('
floor');?></b>
            </li>
        </ul>
		<?php


		the_content();


		understrap_link_pages();
		?>

	</div><!-- .entry-content -->

	<footer class="entry-footer">

		<?php understrap_entry_footer(); ?>

	</footer><!-- .entry-footer -->

</article><!-- #post-## -->
