<?php
/**
 * Single post partial template
 *
 * @package Understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<header class="entry-header">

		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<div class="entry-meta">
			<?php understrap_posted_on(); ?>

		</div><!-- .entry-meta -->

	</header><!-- .entry-header -->

	<?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?>

	<div class="entry-content " >


        <div class="content-city">

		<?php


		the_content();

		?>

        </div>

        <div class="post-list row">
            <?php


            $posts = get_posts( array(
                'numberposts' => 10,
                'category'    => 0,
                'orderby'     => 'date',
                'order'       => 'DESC',
                'meta_key'    => 'metacity',
                'meta_value'  => get_the_ID(),
                'post_type'   => 'realestate',
                'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
            ) );
            if ($posts ) {
                // Start the Loop.
                foreach ( $posts  as $post) {
                    get_template_part( 'loop-templates/content-realestate', get_post_format($post->ID));
                }
            } else {
                get_template_part( 'loop-templates/content', 'none' );
            }
            ?>


        </div>
	</div><!-- .entry-content -->

	<footer class="entry-footer">

		<?php understrap_entry_footer(); ?>

	</footer><!-- .entry-footer -->

</article><!-- #post-## -->
