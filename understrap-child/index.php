<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Understrap
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;

get_header();
$city = get_posts(array(
    'numberposts' => 10,
    'category' => 0,
    'orderby' => 'date',
    'order' => 'DESC',
    'post_type' => 'city',
    'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
));
$realestate = get_posts(array(
    'numberposts' => 20,
    'category' => 0,
    'orderby' => 'date',
    'order' => 'DESC',
    'post_type' => 'realestate',
    'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
));
$type = get_terms('property-type');
$container = get_theme_mod('understrap_container_type');
?>

<?php if (is_front_page() && is_home()) : ?>
    <?php get_template_part('global-templates/hero'); ?>
<?php endif; ?>

    <div class="wrapper" id="index-wrapper">

        <div class="<?php echo esc_attr($container); ?>" id="content" tabindex="-1">

            <div class="row">

                <!-- Do the left sidebar check and opens the primary div -->
                <?php get_template_part('global-templates/left-sidebar-check'); ?>

                <main class="site-main" id="main">
                    <section class="city-section  section">
                        <div class="title-block">
                            <h2>Города</h2>
                        </div>
                        <div class="content row">
                            <?php

                            if ($city) {
                                foreach ($city as $post) {
                                    get_template_part('loop-templates/content-city', get_post_format($post->ID));
                                }
                            } else {
                                get_template_part('loop-templates/content', 'none');
                            }
                            ?>
                        </div>
                    </section>
                    <section class="realestate-section  section">
                        <div class="title-block">
                            <h2>Недвижимость</h2>
                        </div>
                        <div class="content row">
                            <?php

                            if ($realestate) {
                                foreach ($realestate as $post) {

                                    get_template_part('loop-templates/content-realestate');
                                }
                            } else {
                                get_template_part('loop-templates/content', 'none');
                            }
                            ?>
                        </div>
                    </section>
                    <?php
                    if (is_user_logged_in()):
                        ?>
                        <section class="form-section  section">
                            <div class="title-block">
                                <h2>Добавить недвижимость</h2>
                            </div>
                            <div class="content row">
                                <form class="row form-add-realestate"
                                      onsubmit="realestate.add(this); return false">
                                    <div class="inp-block col-12">
                                        <label for="" class="form-label col-12">Заголовок <input type="text"
                                                                                                 name="title"
                                                                                                 class="form-control"
                                                                                                 required></label>
                                    </div>
                                    <div class="inp-block col-12">
                                        <label for="" class="form-label col-12">Площадь <input type="number" name="area"
                                                                                               class="form-control"
                                                                                               required></label>
                                    </div>
                                    <div class="inp-block col-12">

                                        <label for="" class="form-label col-12">Стоимость <input type="number"
                                                                                                 name="price"
                                                                                                 class="form-control"
                                                                                                 required></label>
                                    </div>
                                    <div class="inp-block col-12">
                                        <label for="" class="form-label col-12">Адрес <input type="text" name="address"
                                                                                             class="form-control"
                                                                                             required></label>
                                    </div>
                                    <div class="inp-block col-12">
                                        <label for="" class="form-label col-12">Жилая площадь <input
                                                    class="form-control" type="number" name="living_space"
                                                    required></label>
                                    </div>

                                    <div class="inp-block col-12">
                                        <label for="" class="form-label col-12">Этаж <input type="number"
                                                                                            class="form-control"
                                                                                            name="floor"
                                                                                            required></label>
                                    </div>
                                    <div class="inp-block col-12">
                                        <label for="" class="form-label col-12">Изображение <input type="file"
                                                                                                   class="form-control"
                                                                                                   name="image" required
                                                                                                   accept=".jpg, .jpeg, .png"></label>
                                    </div>
                                    <div class="inp-block select-block col-12">
                                        <label class="form-label col-12">Тип недвижимости
                                        <select name="property[]" class="form-control col-12 " size="3"
                                                multiple="multiple" aria-label="multiple select example">
                                            <?php
                                            foreach ($type as $item):
                                                ?>
                                                <option value="<?= $item->term_id ?>"><?= $item->name ?></option>
                                            <?php endforeach; ?>
                                        </select></label>

                                    </div>
                                    <div class="inp-block select-block col-12">
                                        <label class="form-label col-12">Город
                                        <select name="city" class="form-control col-12">
                                            <?php
                                            foreach ($city as $item):
                                                ?>
                                                <option value="<?= $item->ID ?>"><?= $item->post_title ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        </label>
                                    </div>
                                    <div class="inp-block col-12 d-flex justify-content-center">
                                        <button type="submit" class="btn btn-primary">Отправить</button>

                                    </div>
                                </form>
                            </div>
                        </section>
                    <?php endif; ?>
                </main><!-- #main -->

                <!-- The pagination component -->
                <?php understrap_pagination(); ?>

                <!-- Do the right sidebar check -->
                <?php get_template_part('global-templates/right-sidebar-check'); ?>

            </div><!-- .row -->

        </div><!-- #content -->

    </div><!-- #index-wrapper -->

<?php
get_footer();
