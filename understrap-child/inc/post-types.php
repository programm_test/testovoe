<?php

add_action('init', 'custom_post_types');
if (!function_exists('custom_post_types')) {

    // Register Custom Post Type
    function custom_post_types()
    {

        $labels = array(
            'name' => _x('Недвижимость', 'Post Type General Name'),
            'singular_name' => _x('Недвижимость', 'Post Type Singular Name'),
            'menu_name' => __('Недвижимость'),
            'name_admin_bar' => __('Недвижимость'),
            'archives' => __('Недвижимость'),
            'all_items' => __('Недвижимость'),
            'add_new_item' => __('Добавить недвижимость'),
            'add_new' => __('Добавить недвижимость'),
            'new_item' => __('Добавить недвижимость'),
            'edit_item' => __('Редактировать недвижимость'),
            'update_item' => __('Обновить недвижимость'),
            'view_item' => __('Просмотреть'),
            'view_items' => __('Просмотреть недвижимость'),
            'search_items' => __('Найти недвижимость'),
            'not_found' => __('Не найдено'),
            'not_found_in_trash' => __('Не найдено в корзине'),
            'featured_image' => __('Изображение'),
            'set_featured_image' => __('Установить изображение'),
            'remove_featured_image' => __('Удалить изображение'),
            'use_featured_image' => __('Использовать изображение'),
            'insert_into_item' => __('Вставить в запись'),
            'uploaded_to_this_item' => __('Загрузить в запись'),
            'items_list' => __('Список записей'),
            'items_list_navigation' => __('Записи'),
            'filter_items_list' => __('Фильтровать'),
        );

        $args = array(
            'label' => __('Недвижимость'),
            'labels' => $labels,
            'supports' => array('title', 'custom-fields','thumbnail'),
            'hierarchical' => true,
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_rest' => true,
            'menu_position' => 5,
            'show_in_admin_bar' => true,
            'show_in_nav_menus' => true,
            'menu_icon'=> 'dashicons-admin-home',
            'can_export' => true,
            'has_archive' => true,
            'exclude_from_search' => false,
            'publicly_queryable' => true,
            'capability_type' => 'post',
            'rewrite' => array('slug' => 'realestate', 'with_front' => false)

        );

        register_post_type('realestate', $args);


        $labels = array(
            'name'              => _x('Тип недвижимости', 'taxonomy general name'),
            'singular_name'     => _x('Тип недвижимости', 'taxonomy singular name'),
            'search_items'      => __('Найти тип недвижимости' ),
            'all_items'         => __('Все категории' ),
            'edit_item'         => __('Редактировать'),
            'update_item'       => __('Обновить'),
            'add_new_item'      => __('Добавить'),
            'new_item_name'     => __('Новый тип недвижимости' ),
            'menu_name'         => __('Тип недвижимости' ),
        );

        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array('slug' => 'property-type'),
        );

        register_taxonomy('property-type', array('realestate'), $args );
        $labels = array(
            'name' => _x('Города', 'Post Type General Name'),
            'singular_name' => _x('Города', 'Post Type Singular Name'),
            'menu_name' => __('Города'),
            'name_admin_bar' => __('Города'),
            'archives' => __('Города'),
            'all_items' => __('Города'),
            'add_new_item' => __('Добавить Город'),
            'add_new' => __('Добавить Город'),
            'new_item' => __('Добавить Город'),
            'edit_item' => __('Редактировать Город'),
            'update_item' => __('Обновить Город'),
            'view_item' => __('Просмотреть'),
            'view_items' => __('Просмотреть Город'),
            'search_items' => __('Найти Город'),
            'not_found' => __('Не найдено'),
            'not_found_in_trash' => __('Не найдено в корзине'),
            'featured_image' => __('Изображение'),
            'set_featured_image' => __('Установить изображение'),
            'remove_featured_image' => __('Удалить изображение'),
            'use_featured_image' => __('Использовать изображение'),
            'insert_into_item' => __('Вставить в запись'),
            'uploaded_to_this_item' => __('Загрузить в запись'),
            'items_list' => __('Список записей'),
            'items_list_navigation' => __('Записи'),
            'filter_items_list' => __('Фильтровать'),
        );

        $args = array(
            'label' => __('Города'),
            'labels' => $labels,
            'supports' => array('title', 'thumbnail','editor'),
            'hierarchical' => true,
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_rest' => true,
            'menu_position' => 5,
            'menu_icon'=>'dashicons-location-alt',
            'show_in_admin_bar' => true,
            'show_in_nav_menus' => true,
            'can_export' => true,
            'has_archive' => true,
            'exclude_from_search' => false,
            'publicly_queryable' => true,
            'capability_type' => 'post',
            'rewrite' => array('slug' => 'city', 'with_front' => false)

        );

        register_post_type('city', $args);

    }


}

