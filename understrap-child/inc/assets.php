<?php

add_action( 'wp_enqueue_scripts', 'child_assets' );
function child_assets () {
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/css/theme-child.css' );
    wp_enqueue_script( 'child-script', get_stylesheet_directory_uri() . '/js/script.js', array('jquery'), '1.0.0', false);
}